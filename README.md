# TWITTER CASE STUDY(ON COVID 19) #

PROBLEM STATEMENT :
We have to extract data from Twitter about the ongoing pandemic coronavirus.
Based on the data extracted we have to implement various functionalities like:

1. Need to find the overall number of tweets on coronavirus per country wise 
2. Need to find the overall number of tweets  per country on daily basis
3. Need to find the top 100 words occurring on tweets involving coronavirus
4.Need to find the top 100 words occurring on tweets involving coronavirus per country basis
5. Top 10 preventive/precautionary measures suggested by WHO worldwise
6. Total no.of donations received towards COVID 19  countrywise in all affected countries
7. Ranking of impacted countries over last two months on the weekly basis to see who was standing where at any given week


PREREQUISITES:
Scala: Version 2.12.10
Sbt: Version 0.1
Spark: Version 2.4.5
Kafka: Version 2.4.1
MongoDB: Version 4.2.5


INTRODUCTION:

TWITTER PRODUCER:
The twitter producer gets data from twitter based on some keywords and put them in a kafka topic. 
Tech used: Twitter API & Kafka producer (Scala)

TWITTER CONSUMER:
The Consumer component gets the data from our twitter Kafka topic and insert it into Mongo after  doing some basic transformations. 
Tech used: Spark Streaming (Scala)

API SERVICE:
API service which can be queried using any tool like Postman to give answers to find functionalities.
Tech used: Flask (python), pymongo and pyspark

