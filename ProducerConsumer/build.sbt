name := "CaseStudy"

version := "0.1"

scalaVersion := "2.11.8"
libraryDependencies++=Seq("org.apache.spark" %% "spark-core" % "2.3.2" % "provided",
  "org.apache.spark" %% "spark-sql" % "2.3.2",
  "org.apache.spark" %% "spark-mllib" % "2.3.2",
  "org.apache.kafka" % "kafka-clients" % "2.4.1",
  "org.apache.spark" %% "spark-sql-kafka-0-10" % "2.3.2")


libraryDependencies += "com.googlecode.json-simple" % "json-simple" % "1.1"


libraryDependencies += "org.mongodb.scala" %% "mongo-scala-driver" % "2.9.0"

libraryDependencies += "org.scala-lang.modules" %% "scala-parser-combinators" % "1.0.4"

libraryDependencies += "org.scalatest" %% "scalatest-funsuite" % "3.3.0-SNAP2"

libraryDependencies ++= Seq("junit" % "junit" % "4.8.1" % "test")


// https://mvnrepository.com/artifact/org.apache.kafka/kafka-clients
libraryDependencies += "org.apache.kafka" % "kafka-clients" % "2.0.0"

// https://mvnrepository.com/artifact/org.slf4j/slf4j-simple
libraryDependencies += "org.slf4j" % "slf4j-simple" % "1.7.30" % Test

// https://mvnrepository.com/artifact/com.twitter/hbc
libraryDependencies += "com.twitter" % "hbc-core" % "2.2.0"

// https://mvnrepository.com/artifact/org.slf4j/slf4j-api
libraryDependencies += "org.slf4j" % "slf4j-simple" % "1.7.30"

// https://mvnrepository.com/artifact/com.googlecode.json-simple/json-simple
libraryDependencies += "com.googlecode.json-simple" % "json-simple" % "1.1"

// https://mvnrepository.com/artifact/junit/junit
libraryDependencies += "junit" % "junit" % "4.12" % Test
