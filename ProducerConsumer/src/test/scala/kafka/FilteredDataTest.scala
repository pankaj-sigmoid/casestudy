package kafka

import org.junit.Test
import org.junit.Assert.assertEquals

class FilteredDataTest {

  /**
   * test the format and value of date filtered
   */
  @Test
  def Test_Tweet_Date(): Unit = {
    val tweet1 = s""""Apr 16, 2020","When you die of corona virus and 1hour later they discover the cure in that hospital #Covid_19 ","California, USA""""
    val tweet2 = s""""Mar 20, 2020","Perawat RS Siloam Surabaya Hastuti Yulistiorini meninggal karena Corona.Brumah skait tersebut selama 32 tahun","Indonesia""""
    val tweet3 = s""""@kylegriffin1 Hope it’s riddled w corona virus","Limerick, Ireland""""

    assertEquals("2020-04-16", FilteredData.Filter(tweet1)._1)
    assertEquals("2020-03-20", FilteredData.Filter(tweet2)._1)
    assertEquals("-1", FilteredData.Filter(tweet3)._1)
  }

  /**
   * test the value of location filtered
   */
  @Test
  def Test_Tweet_Location(): Unit = {
    val tweet1 = s""""Apr 16, 2020","When you die of corona virus and 1hour later they discover the cure in that hospital #Covid_19 ","California, USA""""
    val tweet2 = s""""Apr 10, 2020","@kylegriffin1 Hope it’s riddled w corona virus","Limerick, India""""
    val tweet3 = s""""Mar 20, 2020","Perawat RS Siloam Surabaya Hastuti Yulistiorini meninggal karena Corona.Brumah skait tersebut selama 32 tahun","Bengaluru""""
    val tweet4 = s""""Apr 10, 2020","@kylegriffin1 Hope it’s riddled w corona virus","Indian""""
    val tweet5 = s""""Apr 16, 2020","When you die of corona virus and 1hour later they discover the cure in that hospital #Covid_19 ","Madhya Pradesh""""

    assertEquals("United States of America", FilteredData.Filter(tweet1)._2)
    assertEquals("India", FilteredData.Filter(tweet2)._2)
    assertEquals("India",FilteredData.Filter(tweet3)._2)
    assert("India" != FilteredData.Filter(tweet4)._2)
    assertEquals("India", FilteredData.Filter(tweet5)._2)

  }
}
