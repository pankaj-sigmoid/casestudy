package kafka

import org.junit.{Before, Test}
import org.junit.Assert.assertEquals
import scala.io.Source

class ExtractDataTest {

  var lines = Seq[String]()

  /**
   * reading tweets retrieve from twitter from a file
   */
  @Before
  def initialize()={
    lines = Source.fromFile("test_tweets.txt").getLines.toSeq
  }

  /**
   * testing a general tweet containing all type of information
   */
  @Test
  def testTweet()={
    val jsonData = lines(0)
    val output = ExtractData.extractTweetFromJson(jsonData)
    val required: String = "\"Apr 06, 2017\",\"1/ Today we’re sharing our vision for the future of the Twitter API platform!\",\"Internet\""
    assertEquals(output,required)
  }

  /**
   *  testing a tweet in which location field is absent
   */
  @Test
  def testTweetWithoutLocation()={
    val jsonData = lines(1)
    val output = ExtractData.extractTweetFromJson(jsonData)
    val required: String = "-1"
    assertEquals(output,required)
  }

  /**
   *  testing a tweet whose length is more than 140 characters
   *  and full text is inside extended_tweet field
   */
  @Test
  def testTweetWithExtendedTweet()={
    val jsonData = lines(2)
    val output = ExtractData.extractTweetFromJson(jsonData)
    val required: String = "\"May 10, 2018\",\"Just another Extended Tweet with more than 140 characters, generated as a documentation example, showing that [\"truncated\": true] and the presence of an \"extended_tweet\" object with complete text and \"entities\" #documentation\",\"Internet\""
    assertEquals(output,required)
  }

  /**
   *  testing a tweet which is a retweet,
   *  original message is inside retweeted_status field
   */
  @Test
  def testTweetWithRetweetedStatus()={
    val jsonData = lines(3)
    val output = ExtractData.extractTweetFromJson(jsonData)
    val required: String = "\"May 10, 2018\",\"original message\",\"Internet\""
    assertEquals(output,required)
  }

}
