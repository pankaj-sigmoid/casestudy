package kafka

import java.util.{Properties, _}
import java.util.concurrent.{BlockingQueue, LinkedBlockingQueue, TimeUnit}

import com.google.common.collect.Lists
import com.twitter.hbc.ClientBuilder
import com.twitter.hbc.core.{Client, Constants, Hosts, HttpHosts}
import com.twitter.hbc.core.endpoint.StatusesFilterEndpoint
import com.twitter.hbc.core.processor.StringDelimitedProcessor
import com.twitter.hbc.httpclient.auth.{Authentication, OAuth1}

import org.apache.kafka.clients.producer._
import org.apache.kafka.common.serialization.StringSerializer
import org.slf4j.{Logger, LoggerFactory}


object TwitterProducer {

  def main(args: Array[String]): Unit = {
    new TwitterProducer().run()
  }
}

class TwitterProducer {
  // creating a logger
  var logger: Logger = LoggerFactory.getLogger(classOf[TwitterProducer].getName)
  // List of terms which is to be tracked on twitter
  val terms: List[String] = Lists.newArrayList("corona", "COVID-19", "coronavirus")

  def run(): Unit = {
    logger.info("Setup")

    val msgQueue: BlockingQueue[String] = new LinkedBlockingQueue[String](1000)
    // create a twitter client
    val client: Client = createTwitterClient(msgQueue)
    // establish a connection.
    client.connect()
    // create a kafka producer
    val producer: KafkaProducer[String, String] = createKafkaProducer()

    while (!client.isDone) {
      var msg: String = null
      try msg = msgQueue.poll(5, TimeUnit.SECONDS)
      catch {
        case e: InterruptedException => {
          e.printStackTrace()
          client.stop()
        }
      }

      if (msg != null) {
        // info msg to logger
        logger.info(msg)
        //println(msg)
        // extract required information
        val output = ExtractData.extractTweetFromJson(msg)

        // produce output to kafka topic
        producer.send(new ProducerRecord("twitter_topic20", null, output), new Callback() {
          override def onCompletion(recordMetadata: RecordMetadata, e: Exception): Unit = {
            if (e != null)
              logger.error("Something bad happened", e)
          }
        })

      }
    }
    logger.info("End of application")
  }

  var consumerKey: String = "aCUvfhbE7ROZz08zKS8X17KDu"
  var consumerSecret: String = "HFkPdtbhctpgKbLfLZ17fKhOfNLCZhT3FT6UOqK1zL7rwbCV6w"
  var token: String = "941528903629332480-YR4JdeeAmQ78znPP3rRyncdyiC6TI2a"
  var secret: String = "dCTEw8uO1M0mYfiUGJ5ZDkN9HRhG3xyJmfJQU9WXLMFJR"


  /**
   * Creating a Twitter client to establish a connection to twitter,
   * it processes the stream and put individual messages into the provided BlockingQueue
   *
   * @param msgQueue BlockingQueue to fetch the data in
   * @return twitter client
   */
  def createTwitterClient(msgQueue: BlockingQueue[String]): Client = {

    val hosebirdHosts: Hosts = new HttpHosts(Constants.STREAM_HOST)
    val hosebirdEndpoint: StatusesFilterEndpoint = new StatusesFilterEndpoint()
    hosebirdEndpoint.trackTerms(terms)

    val hosebirdAuth: Authentication = new OAuth1(consumerKey, consumerSecret, token, secret)
    val builder: ClientBuilder = new ClientBuilder()
      .name("Hosebird-Client-01")
      .hosts(hosebirdHosts)
      .authentication(hosebirdAuth)
      .endpoint(hosebirdEndpoint)
      .processor(new StringDelimitedProcessor(msgQueue))
    val hosebirdClient: Client = builder.build()
    hosebirdClient
  }


  /**
   * Creating a Kafka Producer with specified properties
   *
   * @return Kafka Producer
   */
  def createKafkaProducer(): KafkaProducer[String, String] = {
    val bootstrapServers: String = "127.0.0.1:9092"
    // create Producer properties
    val properties: Properties = new Properties()
    properties.setProperty(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServers)
    properties.setProperty(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, classOf[StringSerializer].getName)
    properties.setProperty(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, classOf[StringSerializer].getName)
    // create safe Producer
    properties.setProperty(ProducerConfig.ENABLE_IDEMPOTENCE_CONFIG, "true")
    properties.setProperty(ProducerConfig.ACKS_CONFIG, "all")
    properties.setProperty(ProducerConfig.RETRIES_CONFIG, java.lang.Integer.toString(java.lang.Integer.MAX_VALUE))

    properties.setProperty(ProducerConfig.MAX_IN_FLIGHT_REQUESTS_PER_CONNECTION, "5")
    properties.setProperty(ProducerConfig.COMPRESSION_TYPE_CONFIG, "snappy")
    properties.setProperty(ProducerConfig.LINGER_MS_CONFIG, "20")
    // 32 KB batch size
    properties.setProperty(ProducerConfig.BATCH_SIZE_CONFIG, java.lang.Integer.toString(32 * 1024))
    // create the producer
    val producer: KafkaProducer[String, String] = new KafkaProducer[String, String](properties)

    producer
  }

}
