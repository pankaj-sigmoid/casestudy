package kafka
import kafka.FilteredData.Filter
import kafka.WriteToDB.WriteToMongo
import org.apache.log4j.{Level, Logger}
import org.apache.spark.sql.{ForeachWriter, Row, SparkSession}
import org.mongodb.scala.{Document, MongoClient, MongoCollection, MongoDatabase}


object TwitterConsumer {
  // make a connection to a running mongoDB instance
  val mongoClient: MongoClient = MongoClient()
  // get the database
  val database: MongoDatabase = mongoClient.getDatabase("abcd")
  // get a collection of the database
  val collection: MongoCollection[Document] = database.getCollection("tweets")
  if (mongoClient == null) print("Unable to connect to database")


  def main(args: Array[String]): Unit = {
    // create a logger
    Logger.getLogger("org").setLevel(Level.ERROR)

    // create a SparkSession
    val spark = SparkSession
      .builder
      .appName("TwitterTweetsStream")
      .master("local[*]")
      .getOrCreate()

    // Read stream form kafka
    // subscribe to one topic
    val ds = spark.readStream.format("kafka")
      .option("kafka.bootstrap.servers", "localhost:9092")
      .option("subscribe", "twitter_topic20")
      .option("startingOffsets", "earliest")
      .load()

    val selectds = ds.selectExpr("CAST(value AS STRING)")

    // write the output of the streaming Data to mongoDB
    val consumer = new ForeachWriter[Row] {
      // open the connection
      def open(partitionId: Long, version: Long): Boolean = {
        true
      }

      /**
       * extract usefull data from json data,
       * filter data based on country mapping,
       * Write process data to mongo
       *
       * @param record twitter record from kafka topic
       */
      def process(record: Row): Unit = {
        // Write string to connection
        val extractdata = Filter(record(0).toString)
        if (extractdata._1 != "-1") {
          println(extractdata._1, extractdata._2, extractdata._3)
          val Date = extractdata._1
          val location = extractdata._2
          val tweet = extractdata._3

          val doc: Document = Document("date" -> s"$Date", "location" -> s"$location", "tweet" -> s"$tweet")

          WriteToMongo(doc, collection)
        }
      }

      // close the connection when no more tweets in kafka topic
      def close(errorOrNull: Throwable): Unit = {
        println("Tweets Completed.")
      }
    }

    val writedf = selectds.writeStream
      .foreach(consumer)
      .start()
    writedf.awaitTermination()
  }



}


