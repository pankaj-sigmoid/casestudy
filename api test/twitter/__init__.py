
from twitter.main.controllers import main
from twitter.admin.controllers import admin

from flask import Flask, jsonify, request
from pymongo import MongoClient
from nltk.corpus import stopwords

from pyspark.sql.functions import col
from pyspark import SparkContext,SparkConf
from pyspark.sql import SparkSession
from pyspark.sql.functions import explode,split

app = Flask(__name__)
app.config["DEBUG"] = True
app.config["TESTING"] = True

client = MongoClient("mongodb://localhost:27017")
db = client.finalTwitter
tasks_collection = db.tweets

conf = SparkConf().set("spark.jars.packages", "org.mongodb.spark:mongo-spark-connector_2.11:2.3.2")
sc = SparkContext(conf=conf)
spark = SparkSession.builder.appName("myApp") \
    .config("spark.mongodb.input.uri", "mongodb://127.0.0.1/finalTwitter.tweets") \
    .config("spark.mongodb.output.uri", "mongodb://127.0.0.1/finalTwitter.tweets") \
    .getOrCreate()
df = spark.read.format("com.mongodb.spark.sql.DefaultSource").load()

"""
Query to find the total number of documents/tweets in the Database.

:return: A json representation of count of documents.
"""
@app.route('/api/count', methods=['GET'])
def count():
    ans = tasks_collection.count()
    return jsonify(total_count=ans)


"""
Query to find overall number of tweets on coronavirus per country wise.

:return: A json representation of array of documents(ans).
"""
@app.route('/api/country_wise', methods=['GET'])
def query1():
    cursor = tasks_collection.aggregate([
        {'$group':
             {'_id': '$location',
              'count': {'$sum': 1}
              }
         },
        {'$sort': {'count': -1}}
    ])

    ans = []
    for doc in cursor:
        ans.append(doc)
    return jsonify(ans)


"""
Query to find overall number of tweets per country vise on a daily basis.

:return: A json representation of array of documents(ans).
"""
@app.route('/api/daily_basis', methods=['GET'])
def query2():
    cursor = tasks_collection.aggregate([
        {'$group':
             {'_id': {'location': '$location', 'date': '$date'},
              'count': {'$sum': 1}
              }
         },
        {'$sort': {'count': -1}}
    ],
        allowDiskUse=True
    )

    ans = []
    for doc in cursor:
        ans.append(doc)
    return jsonify(ans)


"""
Query to find the top 100 words occurring on tweets involving coronavirus. (words should be nouns/verbs and
not involving common ones like the, is, are, etc etc).

:return: A json representation of array of documents(ans).
"""
@app.route('/api/word_count', methods=['GET'])
def query3():
    ot_stop = ['de', 'la', 'que', 'en', 'el', '&amp;', '-', 'por',
               'del', 'los', 'se', 'e', 'le', 'con', '|', 'es', 'da',
               'una', 'les', 'al', 'em', 'un', 'para', 'las', "", "I",
               "के", "में", "की", "से", "को", "है", "का", "The", "A", "This"]
    stopWords = list(set(stopwords.words('english'))) + ot_stop

    q3 = df.withColumn("word", explode ( split("tweet", " "))).groupBy("word").count().filter((~col('word').isin(stopWords))).sort(col("count").desc())

    res=q3.collect()[:100]
    #print(type(res))

    return jsonify(res)


"""
Query to find the top 100 words occurring on tweets involving coronavirus on a per country basis.

:return: A json representation of array of documents(ans).
"""
@app.route('/api/country_wise_word_count', methods=['GET'])
def query41():
    ot_stop = ['de', 'la', 'que', 'en', 'el', '&amp;', '-', 'por',
               'del', 'los', 'se', 'e', 'le', 'con', '|', 'es', 'da',
               'una', 'les', 'al', 'em', 'un', 'para', 'las', "", "I",
               "के", "में", "की", "से", "को", "है", "का", "The","A", "This"]
    stopWords = list(set(stopwords.words('english'))) + ot_stop
    req_data = request.get_json()
    country_list = req_data[0]["country"]
    val4_sp=[]
    for country in country_list:
        q4_spark=df.filter(col('location')==country).withColumn("word", explode ( split("tweet", " "))).groupBy("word").count().filter((~col('word').isin(stopWords))).sort(col("count").desc())
        val4_spark=q4_spark.collect()[:100]
        val4_sp.append({country:val4_spark})

    return jsonify(val4_sp)


@app.route('/api/query4', methods=['GET'])
def query4():
    ot_stop = ['de', 'la', 'que', 'en', 'el', '&amp;', '-', 'por',
               'del', 'los', 'se', 'e', 'le', 'con', '|', 'es', 'da',
               'una', 'les', 'al', 'em', 'un', 'para', 'las', "", "I",
               "के", "में", "की", "से", "को", "है", "का", "The","A", "This"]
    en_stops = list(set(stopwords.words('english'))) + ot_stop
    cursor = tasks_collection.aggregate([
        {"$project": {"location": "$location", "word": {"$split": ["$tweet", " "]}}},
        {"$unwind": "$word"},
        {'$project': {'word': {'$toLower': "$word"}, 'location': 1}},
        {'$match': {
            '$and': [
                {'word': {'$ne': ""}},
                {'word': {'$nin': en_stops}}
            ]}},
        {"$group": {"_id": {"location": "$location", "word": "$word"}, "total": {"$sum": 1}}},
        {"$sort": {"total": -1}},
        {"$group": {"_id": "$_id.location", "Top_Words": {"$push": {"word": "$_id.word", "total": "$total"}}}},
        {"$project": {"location": 1, "top100Words": {"$slice": ["$Top_Words", 100]}}}
    ], allowDiskUse=True)

    ans = []
    for doc in cursor:
        ans.append(doc)
    return jsonify(ans)

# @app.route('/api/query4', methods=['GET'])
# def query4():
#     ot_stop = ['de', 'la', 'que', 'en', 'el', '&amp;', '-', 'por',
#                'del', 'los', 'se', 'e', 'le', 'con', '|', 'es', 'da',
#                'una', 'les', 'al', 'em', 'un', 'para', 'las', "", "I"]
#     stopWords = list(set(stopwords.words('english'))) + ot_stop
#
#     wordswithcountry= df.groupByKey().map(x=>(x._1,x._2.flatMap(y=>y.split("\\W+")))).map(x=>(x._1,x._2.filter(!badWords.contains(_)))).map(x=>(x._1,x._2)).flatMap{case(k,v)=>v.map(v=>(k,v))}
#
#     df.createOrReplaceTempView("table")
#     r= spark.sql("select location,tweet group by location order by location asc")
#     r.createOrReplaceTempView("data")
#     t=spark.sql("select location, tweet, Count, ROW_NUMBER() over(partition by location order by location asc, Count desc) as RowNum from data")
#     t.createOrReplaceTempView("new")
#     v=spark.sql("select * from new where RowNum<=4 order by location")
#
#     res = r.collect()[:100]
#     return jsonify(res)


"""
Query to find Top 10 preventive / precautionary measures suggested by WHO worldwide /country wise

:returns: A json representation of array of documents(ans) which is passed an argument.
"""
@app.route('/api/precautions', methods=['GET'])
def query5():
    WHO_db = client.WHOSEARO
    tweets = WHO_db.tweets
    cursor = tweets.find( { "$text": { "$search": "\"Here are some simple measures that you can adopt to prevent the spread of #coronavirus:\"" } },{"tweet":1,"_id":0})
    ans = []
    for doc in cursor:
        ans.append(doc)
    #return jsonify(ans)

    cursor1 = tweets.aggregate(
        [{"$match": {"$text": {"$search": "\"protect yourself\" -dengue -Hepatitis -Influenza -syphilis -HIV -leptospirosis -foodborne -DelhiPollution -mosquito -SafeFood"}}},
         {"$project":  {"tweet": 1, "_id": 0}}
         ],allowDiskUse=True)

    for doc in cursor1:
        ans.append(doc)
    return jsonify(ans)


"""
Query to find the total no. of donations received towards COVID-19 country wise, in all the affected countries

:returns: A json representation of array of documents(ans) which is passed an argument.
"""
@app.route('/api/donations', methods=['GET'])
def query6():
    cursor = tasks_collection.aggregate([
        { "$match": { "$text": { "$search": "donations" } } },
        { "$group": { "_id": "$location", "count": { "$sum": 1 } } },
        { "$sort"  : { "count" : -1 }  }, {"$sort": {"count": -1}}
    ])
    ans = []
    for doc in cursor:
        ans.append(doc)
    return jsonify(ans)


"""
Query to find the ranking of impacted countries over the last 2 month on a week basis, to see who was standing
where at any given week

:returns: A json representation of array of documents(ans) which is passed an argument.
"""
#INCOMPLETE

@app.route('/api/rankings', methods=['GET'])
def query7():
    cursor = tasks_collection.aggregate([{ "$match": { "$text": { "$search": "corona" } } }, { "$group": { "_id": "$location", "count": { "$sum": 1 } } },{ "$sort" : { "count" : -1 } },{"$project":{"_id":1}} ])
    ans = []
    for doc in cursor:
        ans.append(doc)
    return jsonify(ans)

